var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var jwt = require("jsonwebtoken");
var session = require('express-session');
//var MongoStore = require('connect-mongo')(session);
var expressValidator = require('express-validator');
var passport = require('passport');
var passportJWT = require('passport-jwt');
var BearerStrategy = require('passport-http-bearer');
//var LocalStrategy = require('passport-local').Strategy;
var mongoose = require('mongoose');
var WebSocket = require('ws');
let server = require('http').createServer();

var routes = require('./routes/index');
var articleRoutes = require('./routes/articles');
var userRoutes = require('./routes/users');
var chat = require('./controller/chat');

var app = express();
mongoose.connect('localhost:27017/articleapp');

var initPassport = require('./passport-init');
initPassport(passport);

app.use(passport.initialize());


// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(expressValidator());
app.use(cookieParser());


app.use(passport.session());
//app.use(ejwt({secret: 'secret'}).unless({ path: ['/auth', '/list', '/login'] }));

app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type, Accept, Authorization');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});

app.use('/index', routes);
app.use('/articles', articleRoutes);
app.use('/users', userRoutes);

chat.create(server);

//app.use(express.static(path.join(__dirname, '../client')));
//app.use(express.static(path.join(__dirname, '../client/src'), {index: 'index.html'}))
console.log('env=' + app.get('env'));



server.on('request',app);

server.listen(3001, function () {
  console.log('Example app listening on port 3001!');
});

module.exports = app;
