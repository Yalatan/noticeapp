
var express = require('express');
var router = express.Router();
var ObjectId = require('mongoose').Types.ObjectId;
var passport = require('passport');
var multer  = require('multer');
var fs = require('fs');
var sharp = require('sharp');
var mongoose = require("mongoose");
var Grid = require('gridfs-stream');
var conn = mongoose.connection;
Grid.mongo = mongoose.mongo;

var Article = require('../models/articles');
var passportinit = require('../passport-init');
var User = require('../models/users');
var upload = multer({ dest: 'new/' });
var ObjectId = require('mongodb').ObjectID;


conn.once('open', function () {
  var gfs = Grid(conn.db);
  router.post('/new',
    upload.any(),
    passport.authenticate('bearer', { session: false }),
    (req, res, next) => {

      var articleToParse = JSON.parse(req.body.article);
      var article = new Article({
        nameArticle: articleToParse.nameArticle,
        content: articleToParse.content,
        user: req.user._id,
        //files: req.files,
        //imagesId: []
      });


      let promises = req.files.map( file => {
        return new Promise((resolve, reject) => {
          var writeStream = gfs.createWriteStream({
            filename: file.filename,
            content_type: file.mimetype
          });
          writeStream.on('close', (file) => {
            resolve(file);
          });
          fs.createReadStream(file.path).pipe(writeStream);
          //promises.push(promise);
        });
       promises.push(promise);
      });

      User.findById(req.user._id, function(err, user){
        user.articles.push(article);
        user.save();
      });

      Promise.all(promises).then( result => {
        article.imagesId = result.map( x => x._id);
        article.save(function (err, result) {
          if (err) {
            return res.status(404).json({
              title: 'An error has occurred',
              error: err
            });
          }
          res.status(201).json({
            message: 'Saved article',
            obj: result
          });

        });
      })
        .catch(error => {
          alert(error);
        });


    });
});




router.get('/list', function (req, res, next) {
    var currentpage = parseInt(req.query.currentpage);
    var articlesPerPage = parseInt(req.query.articlesPerPage);
    Article.find()
        .skip(articlesPerPage*(currentpage-1))
        .limit(articlesPerPage)
        .exec(function(err, articles) {

            var callback = function(err, articlesTotal) {
                var result = {
                    articles: articles,
                    articlesTotal: articlesTotal
                }
                res.send(result);
            };
            Article.count({}, callback);
        });



});


router.get('/:id', function (req, res, next) {

    Article.findById(req.params.id)
        .populate({path: 'user', select: 'username'})
        .exec(function (err, article) {
            res.send(article);
            if (err) return handleError(err);
    });
});


router.get('/getImage/:id', function (req, res, next) {

  var gfs = Grid(conn.db);
  gfs.findOne({ _id: req.params.id}, function(err, files) {

      var data = [];
    //for (var i=0; i < article.imagesId.length; i++) {



    gfs.exist({ _id: req.params.id }, function(err, found) {
      if (err) {
        handleError(err);
        return;
      }

      if (!found) {
        res.send('Error on the database looking for the file.')
        return;
      }

      var readstream = gfs.createReadStream({
        _id: new ObjectId(req.params.id)
      });

      readstream.on('data', function (chunk) {
        data.push(chunk);
      });
      readstream.on('end', function () {
        data = Buffer.concat(data);
        sharp(data)
          .resize(parseInt(req.query.w), parseInt(req.query.h))
          .toBuffer(function (err, data) {
            res.set('Content-Type', gfs.files.contentType);
            res.send(data);
          });

      });

      readstream.on('error', function (err) {
        console.log('An error occurred!', err);
        res.send(err);
      });
    //}
    });
  });
});

conn.once('open', function () {
  var gfs = Grid(conn.db);
  router.post('/edit/:id',
    upload.any(),
    passport.authenticate('bearer', { session: false }),
    (req, res, next) => {
      var articleToParse = JSON.parse(req.body.article);
      let imagesToDeleteIds = JSON.parse(req.body.imagesToDeleteIds);
      Article.findOne({_id: req.params.id}, function (err, article) {
        //let article = articleFromDB;
        article.nameArticle = articleToParse.nameArticle;
        article.content = articleToParse.content;


        let promises = req.files.map( file => {
            return new Promise((resolve, reject) => {
              var writeStream = gfs.createWriteStream({
                filename: file.filename,
                content_type: file.mimetype
              });
              writeStream.on('close', (file) => {
                resolve(file);
              });
              fs.createReadStream(file.path).pipe(writeStream);
            });
          });


        Promise.all(promises).then( result => {
          if (imagesToDeleteIds) {
            article.imagesId = article.imagesId.filter(el => !imagesToDeleteIds.includes(el.toString()));
            for (var i = 0; i < imagesToDeleteIds.length; i++) {
              gfs.remove({ _id: imagesToDeleteIds[i]}, function (err) {
                if (err) return handleError(err);
                console.log('success');
              });
            }
           // article.imagesId.pull(imagesToDeleteIds);
          }
          article.imagesId = article.imagesId.concat(result.map( x => x._id));
          article.save(function (err, result) {
            if (err) {
              return res.status(404).json({
                title: 'An error has occurred',
                error: err
              });
            }
            res.status(201).json({
              message: 'Saved article',
              obj: result
            });

          });
        })
          .catch(error => {
            console.log(error);
          });
        //article.save();
      });


    });
});

router.post('/delete/:id', function (req, res, next) {
  var gfs = Grid(conn.db);

  Article.findOne({ _id: req.params.id }, function(err, article) {
    for (var i = 0; i < article.imagesId.length; i++) {
      gfs.remove({_id: article.imagesId[i]}, function (err) {
        if (err) return handleError(err);
        console.log('success');
      });
    }
      article.remove(function(err) {
        if (err) {
          return res.status(404).json({
            title: 'An error has occurred',
            error: err
          });
        }
        res.status(201).json({
          message: 'Deleted article'
        });

      });

  });
});




module.exports = router;
