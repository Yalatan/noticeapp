var express = require('express');
var router = express.Router();
var passport = require('passport');
var User = require('../models/users');
var passportinit = require('../passport-init');



router.get('/auth',
    passport.authenticate('bearer', { session: false }),
    function(req, res) {
            res.json(req.user);
    });



// router.get('/auth', passport.authenticate('jwt', { session: false }),  function(req, res) {
//
//         res.send(req.user);
//
// });



// router.get('/auth', ensureAuthorized, function(req, res) {
//     User.findOne({token: req.token}, function(err, user) {
//         if (err) {
//             res.send(err);
//         } else {
//             res.send(req.user);
//         }
//     });
// });
//
// function ensureAuthorized(req, res, next) {
//     var bearerToken;
//     var bearerHeader = req.headers["authorization"];
//     if (typeof bearerHeader !== 'undefined') {
//         var bearer = bearerHeader.split(" ");
//         bearerToken = bearer[2];
//         req.token = bearerToken;
//         next();
//     } else {
//         res.send(403);
//     }
// }


module.exports = router;
