var express = require('express');
var router = express.Router();
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var jwt = require("jsonwebtoken");
var mongoose = require('mongoose');
var multer  = require('multer');
var fs = require('fs');
var sharp = require('sharp');
var Grid = require('gridfs-stream');
var nodemailer = require('nodemailer');
//var sgTransport = require('nodemailer-sendgrid-transport');
var conn = mongoose.connection;
Grid.mongo = mongoose.mongo;


var User = require('../models/users');
var Message = require('../models/message');
var upload = multer({ dest: 'new/' });
var ObjectId = require('mongodb').ObjectID;
var secret = 'secret';

//
// var options = {
//   auth: {
//     api_user: "xxx",
//     api_key: "xxx"
//   }
// };

var client = nodemailer.createTransport({
  host: 'smtp.gmail.com',
  port: 587,
  secure: false,
  requireTLS: true,
  auth: {
    user: 'devprojectnata@gmail.com',
    pass: 'natadevproject'
  }
});





router.post('/signup', function(req, res, next) {
    User.findOne({
        email: req.body.email
    }, function (err, user) {
        if (user) {
            return res.status(500).json({
                msg: 'email exist'
            })
        } else {
            var user = new User({
                username: req.body.username,
                email: req.body.email,
                password: req.body.password
            });
            req.check('username', 'Username is invalide').isLength({min: 3});
            req.check('username', 'Name is required').notEmpty();
            req.check('email', 'Email is required').notEmpty();
            req.check('password', 'Password is required').notEmpty();
            var errors = req.validationErrors();
            if (errors) {
                    return res.status(400).json({
                        title: 'An error has occurred',
                        error: errors
                    });
                }
        user.save(function(err, result) {
            if (err) {
                return res.status(409).json({
                    title: 'An error has occurred',
                    error: err
                });
            }
            res.status(201).json(
          {username: user.username, email: user.email}
            );
        });
    }
    })

});




router.post('/login', function(req, res, next) {
    User.findOne({
        username: req.body.username,
        password: req.body.password
    }, function (err, user) {
        if (err) throw err;

        if (!user) {
            res.status(401).json({
                msg: 'invalid username or password'
            })
        } else {
            var payload = {id: user._id};
            var token = jwt.sign(payload, 'secret', {expiresIn: 10080}); // in seconds
           // user.token = token;
            res.status(200).json({user: user, token: token});
        }
    });
});



conn.once('open', function () {
  var gfs = Grid(conn.db);
  router.post('/edit/:id',
    upload.single('image'), function(req, res, next) {
    var userToParse = JSON.parse(req.body.user);
    User.findOne({_id: req.params.id}, function (err, user) {
      user.username = userToParse.username;
      user.email = userToParse.email;


      let promise = new Promise((resolve, reject) => {
          let file = req.file;
          var writeStream = gfs.createWriteStream({
            filename: file.filename,
            content_type: file.mimetype
          });
          writeStream.on('close', (file) => {
            resolve(file);
          });
          fs.createReadStream(file.path).pipe(writeStream);


      });


      promise.then( result => {
        user.avatarId = result._id;
        user.save(function (err, result) {
          if (err) {
            return res.status(404).json({
              title: 'An error has occurred',
              error: err
            });
          }
          res.status(201).json({
            message: 'Saved changes',
            obj: result
          });

        });
      })
        .catch(error => {
          alert(error);
        });


    });

  });
});





router.post('/logout', function (req, res) {
    req.logOut();
    res.send(200);
});


router.post('/chatlist', function(req, res, next) {
  var user = req.body.userId;

  Message

    .aggregate(
      [ { $match : { ownerPageId : user } },
        {$project: { _id: 0, uniqueValue: {$cond: {if: {"$eq": [ "$from", user ]}, then: "$to", else: "$from" }}}},
        { $match : { uniqueValue: {'$ne': user} } },
        {"$group": { "_id": { "uniqueValue": "$uniqueValue" } } },
        {
          $lookup: {
            from: "users",
            localField: "_id.uniqueValue",
            foreignField: "_id",
            as: "message_docs"
          }
        },
        {"$project": {"guestUser":"$message_docs.username", _id:1}}
      ]
    )

    .exec(function (err, doc) {
      console.log(doc);

        res.send(doc);

    });


});

router.post('/chatUser', function(req, res, next) {
     var user = req.body.userId;
     var chatUserId = req.body.chatUserId;
  Message.find({
    $and: [
      { ownerPageId: user },
      { $or: [{from: chatUserId}, {to: chatUserId}] }
    ]
  })
    .populate({path: 'from', select: {'username': 1, 'avatarId': 1}})
    .populate({path: 'to', select: {'username': 1, 'avatarId': 1}})
    // .populate({path: 'to', select: 'username'})
    .exec(function (err, chat) {
      res.send(chat);
      if (err) return handleError(err);
    });

});

router.post('/user', function(req, res, next) {
  var user = req.body.userId;

  User.findOne({_id: user})
    //.select('username')
    .exec(function (err, user) {
      res.send(user);
      if (err) return handleError(err);
    });

});

router.post('/currentUserChat', function(req, res, next) {
  var currentUser = req.body.currentUser;
  var ownerPageUser = req.body.ownerPageId;


  Message.find({
    $and: [
      { ownerPageId: ownerPageUser },
      { $or: [{from: currentUser}, {to: currentUser}] }
    ]
  })
    .populate({path: 'from', select: 'username'})
    .populate({path: 'to', select: 'username'})
    .exec(function (err, chat) {
      res.send(chat);
      if (err) return handleError(err);
    });

});


router.get('/getImage/:id', function (req, res, next) {

  var gfs = Grid(conn.db);
  gfs.findOne({ _id: req.params.id}, function(err, files) {

    var data = [];

    gfs.exist({ _id: req.params.id }, function(err, found) {
      if (err) {
        handleError(err);
        return;
      }

      if (!found) {
        res.send('Error on the database looking for the file.')
        return;
      }

      var readstream = gfs.createReadStream({
        _id: new ObjectId(req.params.id)
      });

      readstream.on('data', function (chunk) {
        data.push(chunk);
      });
      readstream.on('end', function () {
        data = Buffer.concat(data);
        sharp(data)
          .resize(parseInt(req.query.w), parseInt(req.query.h))
          .toBuffer(function (err, data) {
            res.set('Content-Type', gfs.files.contentType);
            res.send(data);
          });

      });

      readstream.on('error', function (err) {
        console.log('An error occurred!', err);
        res.send(err);
      });
      //}
    });
  });
});

router.post('/restorePassword', function(req, res, next) {
  User.findOne({
    username: req.body.name,
    email: req.body.email
  }, function (err, user) {
    if (err) {
      res.json({success: false, message: err})
    }

    if (!user) {
      res.json({
        message: 'invalid username or password'
      })
    } else {
      user.resetToken = jwt.sign({username: user.username, email: user.email}, secret, {expiresIn: '24h'});
      user.save(function(err) {
        if (err) {
          res.json({success: false, message: err });
        } else {
          var email = {
            from: 'devprojectnata@gmail.com',
            to: user.email,
            subject: 'Localhost Reset PasswordRequest',
            text: 'Hello' + user.username + ', thank you for registering at localhost.com. Please, click on the following link to complete your activation: http://localhost:4200/' + user.resetToken,
            html: 'Hello <strong>' + user.username + '</strong>, <br><br>You recently request a password reset link/ Please click on the link below to reset your password: <br><br><a href="http://localhost:4200/newpassword/' + user.resetToken + '">http://localhost:4200/newpassword</a>'

          };
          client.sendMail(email, function (err, info) {
            res.json({success: true, message: 'Please check your e-mail'});

          });
        }

      });


    }
  });

});

router.get('/sendToken/:token', function(req, res, next) {
  let token = req.params.token;
    User.findOne({
    resetToken: token}, function (err, user) {
    if (err) {
      res.json({success: false, message: err})
    } else {
      res.json({success: true, user: user.email})
    }
  });
});




router.post('/resetPassword', function() {


});



module.exports = router;

