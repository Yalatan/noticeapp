var express = require('express');
//var router = express.Router();
//var ObjectId = require('mongoose').Types.ObjectId;
var mongoose = require("mongoose");
var WebSocket = require('ws');
var passport = require('passport');
var passportinit = require('../passport-init');
var jwt = require('jsonwebtoken');

var Article = require('../models/articles');
var User = require('../models/users');
var Message = require('../models/message');


var sockets = {};

function create (server) {
    const wss = new WebSocket.Server({
      server: server
    });



  wss.on('connection', function connection(ws) {


    var token = ws.upgradeReq.headers['sec-websocket-protocol'];
    var decodedUser = jwt.decode(token);
    sockets[decodedUser.id] = ws;


    ws.on('message', function incoming(message) {

        console.log('received: %s', message);

      var messageToParse = JSON.parse(message);


            var message = new Message({
              content: messageToParse.message,
              to: messageToParse.recipient,
              from: decodedUser.id,
              ownerPageId: messageToParse.ownerPageId
            });


            message.save(function (err, message) {
              Message.findById(message._id)
                .populate('from', 'username')
                .populate('to', 'username')
                .exec(function (err, message) {
                  ws.send(JSON.stringify(message));
                  if (sockets[messageToParse.recipient]) {
                    sockets[messageToParse.recipient].send(JSON.stringify(message));
                  }
                });

            });


    });


  });

 }

module.exports = {create: create};


