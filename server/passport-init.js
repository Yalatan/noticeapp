var passport = require("passport");
var passportJWT = require("passport-jwt");
var users = require("./routes/users.js");
var _ = require('lodash');
var BearerStrategy = require('passport-http-bearer').Strategy;
var User = require('./models/users');
var jwt = require("jsonwebtoken");
//var mongoose = require('mongoose');


// var ExtractJwt = passportJWT.ExtractJwt;
// var Strategy = passportJWT.Strategy;
// var params = {
//     secretOrKey: "secret",
//     jwtFromRequest: ExtractJwt.versionOneCompatibility({authScheme: 'Bearer'})
// };
//
// module.exports = function() {
//     var strategy = new Strategy(params, function(payload, done) {
//         var user = _.find(users,{'id': payload.id});
//         if (user) {
//             return done(null, user);
//         } else {
//             return done(new Error("User not found"), null);
//         }
//     });
//     passport.use(strategy);
//     return {
//         initialize: function() {
//             return passport.initialize();
//         },
//         authenticate: function() {
//             return passport.authenticate("jwt", cfg.jwtSession);
//         }
//     };
// };
module.exports = function() {

    passport.use(new BearerStrategy(
        function (token, done) {
            decoded = jwt.decode(token, 'secret');
            User.findById({_id: decoded.id}, function (err, user) {

                if (err) {
                    return done(err);
                }
                if (!user) {
                    return done(null, false);
                }
                return done(null, user);
            });
        }
    ));


    // passport.serializeUser(function (user, done) {
    //     done(null, user);
    // });
    //
    // passport.deserializeUser(function (user, done) {
    //     done(null, user);
    // });

}