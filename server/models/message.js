var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var shortId = require('shortid');
mongoose.set('debug', true);

var schema = new Schema({
  ownerPageId: {type: String, ref: 'User'},
  content: {type: String},
  from: {type: Schema.Types.String, ref: 'User'},
  to: {type: Schema.Types.String, ref: 'User'},

  //roomId: {type: String},
  time_created:  { type: Date, default: Date.now },

  //conversationId: {type: String, ref: 'Conversation'},
 });

module.exports = mongoose.model('Message', schema);

