var mongoose = require('mongoose');
var shortId = require('shortid');
var Schema = mongoose.Schema;

var schema = new Schema({
    nameArticle: {type: String, require: true},
    content: {type: String, require: true},
    imagesId: [{type: Schema.Types.ObjectId, ref: "fs.files"}],

    user: {type: String, ref: 'User'},
    _id: {
        type: String,
        unique: true,
        default: shortId.generate
    }
});

module.exports = mongoose.model('Article', schema);
