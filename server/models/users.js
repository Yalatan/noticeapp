var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var shortId = require('shortid');
var mongooseUniqueValidator = require('mongoose-unique-validator');

var schema = new Schema({
    username: {type: String, require: true, unique: true},
    email: {type: String, require: true, unique: true},
    avatarId: {type: Schema.Types.ObjectId, ref: "fs.files"},
    imagesId: [{type: Schema.Types.ObjectId, ref: "fs.files"}],
    password: {type: String, require: true, select: false},
    articles: [{type: Schema.Types.String, ref: 'Article'}],
    _id: {
        type: String,
        unique: true,
        default: shortId.generate
    },
    role: {type: String, require: true, default: 'user'},
    resetToken: {type: String}
});
schema.plugin(mongooseUniqueValidator);


var User = module.exports = mongoose.model('User', schema);



