import { NgModule }       from '@angular/core';
import { BrowserModule  } from '@angular/platform-browser';
import {InterceptorService, provideInterceptorService} from 'ng2-interceptors';
//import {HttpModule, Http, XHRBackend, RequestOptions}       from '@angular/http';
import {RouterModule, Routes} from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
//import { ReactiveFormsModule} from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ServerURLInterceptor } from './services/serverUrlInterceptor';
import {HTTP_INTERCEPTORS, HttpClient} from '@angular/common/http';

import { ModalModule } from 'ngx-bootstrap/modal';

import { NavbarComponent } from './shared/navbar.component';
import { AppComponent }   from './app.component';
import {UsersService} from './services/users.service';
import {ChatService} from './services/chat.service';
import {WebSocketService} from './services/websocket.service';
import {AppRoutingModule} from "./app.routes";
import { ArticlesListComponent }   from './articles/articles-list/articles-list.component';
import { LoginComponent } from './users/login/login.component';
import { ForgetPasswordComponent } from './users/forgetPassword/forgetPassword.component';
import  { LoginModalComponent } from './users/login/login.modal';
import {ArticlesService} from "./services/articles.service";
//import {HttpInterceptor} from "./httpinterceptor";
//import { PSService, InterceptHttp } from "ng2-http-intercept";
import {NewArticleComponent} from "./articles/new-article/new-article.component";
import { EditArticleComponent }    from './articles/edit-article/edit-article.component';
import {SignupComponent} from "./users/signup/signup.component";
import {ProfileComponent} from "./users/profile/profile.component";
import {ChatComponent} from "./users/chat/chat.component";
import {CreateMessageComponent} from "./users/chat/create-message.component";
import {LocationStrategy, HashLocationStrategy} from '@angular/common';
import {ArticleDetailComponent} from "./articles/article-detail/article-detail.component";
import {EditUserComponent} from "./users/edit/edit-user.component";
import { DraggableDirective } from './users/profile/draggable.directive';
import { TokenInterceptor } from './services/token.interceptor';
import {ResetPasswordComponent} from "./users/resetPassword/resetPassword.component";



@NgModule({
  declarations: [AppComponent, NavbarComponent, ArticlesListComponent, LoginComponent, ForgetPasswordComponent, ResetPasswordComponent, NewArticleComponent, EditArticleComponent, LoginModalComponent, SignupComponent, ProfileComponent, ChatComponent, CreateMessageComponent, ArticleDetailComponent, EditUserComponent, DraggableDirective],
  imports: [ ModalModule.forRoot(), HttpClientModule, BrowserModule, RouterModule, FormsModule, ReactiveFormsModule, AppRoutingModule],
  bootstrap: [AppComponent],


  providers: [UsersService, ArticlesService, ChatService, WebSocketService,

    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ]
})
export class AppModule {}
