import { NgModule }              from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ArticlesListComponent }   from './articles/articles-list/articles-list.component';
import {SignupComponent} from "./users/signup/signup.component";
import {NewArticleComponent} from "./articles/new-article/new-article.component";
import { EditArticleComponent }    from './articles/edit-article/edit-article.component';
import { LoginComponent } from './users/login/login.component';
import { ForgetPasswordComponent } from './users/forgetPassword/forgetPassword.component';
import {ProfileComponent} from "./users/profile/profile.component";
import { ModuleWithProviders } from '@angular/core';
import {ArticleDetailComponent} from "./articles/article-detail/article-detail.component";
import {EditUserComponent} from "./users/edit/edit-user.component";
import {ResetPasswordComponent} from "./users/resetPassword/resetPassword.component";

// const indexRoute = {path: 'articles', component: ArticlesComponent};
// const fallbackRoute = {path: '**', component: LoginComponent}


const appRoutes: Routes = [

  { path: '', redirectTo: '/articles', pathMatch: 'full'},

  { path: 'articles', component: ArticlesListComponent, children: [
    // { path: ''},
    { path: 'new', component: NewArticleComponent },
    { path: 'edit/:id', component: EditArticleComponent },
    { path: ':id', component: ArticleDetailComponent }


  ]},

  { path: 'users/signup', component: SignupComponent },
  { path: 'users/login', component: LoginComponent },
  { path: 'user/forgetPassword', component: ForgetPasswordComponent },
  { path: 'newpassword/:token', component: ResetPasswordComponent },
  { path: 'users/profile/edit/:id', component: EditUserComponent },
  { path: 'users/profile/:id', component: ProfileComponent },
  { path: '**', component: ArticlesListComponent },




];
@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule {}

