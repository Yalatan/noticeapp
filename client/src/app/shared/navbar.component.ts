import {Component, OnInit, Input} from '@angular/core';
import {  Router } from '@angular/router';

import {UsersService} from "../services/users.service";


@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']

 })

export class NavbarComponent implements OnInit{


  @Input() user;
  token: boolean;
  subscription;

  constructor(private _usersService: UsersService,
              private _router: Router) {
    this._usersService.user.subscribe(value => { this.user = value });
  }

  ngOnInit() {
    // this.subscription = this._usersService.currentuser.subscribe(value =>
    //     this.user = value);
  }

  logout() {
    this.token = null;
    localStorage.removeItem('token');
    this._usersService.user.next(undefined);
    this._router.navigate(['/articles']);
  }
}
