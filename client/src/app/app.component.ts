
import { Component, ViewContainerRef, OnInit } from '@angular/core';
import {BehaviorSubject} from "rxjs";

import { UsersService } from "./services/users.service";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styles:
    [`.container {
    padding-top: 70px;
  }`]


})

export class AppComponent implements OnInit {
  user;
  subscription;
  token;
  username;

  constructor(private viewContainerRef: ViewContainerRef,
              private _usersService: UsersService) {

    this.viewContainerRef = viewContainerRef;
    //this.subscription = this._usersService.user.subscribe(value => this.user = value);
    // this.subscription = this._usersService.currentuser.subscribe(value =>
    //     this.user = value);

  }

  ngOnInit() {
    var token = localStorage.getItem('token');
    if (token) {
      //this._usersService.setLoginStatus(true);
      this._usersService.getUser()
        .subscribe(user => {
          this.user = user;
//             this.username = user.username;
        });
    }
    else {
      this._usersService.user.next(undefined);
    }


  }


}
