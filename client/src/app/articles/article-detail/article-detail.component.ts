import {Component, OnInit} from '@angular/core';
import {ArticlesService} from "../../services/articles.service";
import {ActivatedRoute, Router} from "@angular/router";
import {UsersService} from "../../services/users.service";


@Component({
  selector: 'article-detail',
  templateUrl: './article-detail.component.html'

})

export class ArticleDetailComponent implements  OnInit {
  articleIndex;
  selectedArticle;
  user;
  chatMessages;

  constructor(
    private articlesService: ArticlesService,
    private _usersService: UsersService,
    private route: ActivatedRoute,
    private _router: Router) {

       this._usersService.user.subscribe(value =>
      this.user = value);

  }

  ngOnInit() {
    this.route.params.subscribe(
      (params: any) => {
        this.articleIndex = params['id'];
        this.selectedArticle = this.articlesService.getArticle(this.articleIndex)
          .subscribe(article => {
            this.selectedArticle = article
          });
      });

  }

  getUserData (userId) {
       let link = ['users/profile', userId];
    this._router.navigate(link);

  }

}
