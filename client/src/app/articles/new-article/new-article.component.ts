
import {Component, OnInit, ViewChild, Renderer} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import {Http, Headers, RequestOptions} from "@angular/http";
import { Router } from '@angular/router';
import {ElementRef} from '@angular/core';



import { ArticlesService } from "../../services/articles.service";
import {UsersService} from "../../services/users.service";

import {Observable} from "rxjs";
import {Input} from "@angular/core/src/metadata/directives";


export class ImageFile{
  public constructor(
    fields?: {
      image?: any,
      file?: File
    }) {
    if (fields) Object.assign(this, fields);
  }
  image: any;
  file: File;
}

@Component({
  templateUrl: './new-article.component.html',
  styles: [`
        .chooseImage {
           margin-bottom: 10px;
        }
       
        .picPreview {
           margin: 10px;
            float: left;
           
        }
         .glyphicon-remove {
           position: relative;
           top: -40px;
           left: -17px;
           z-index: 1;
           cursor:pointer;
           position: relative;
        }
        #image {
        z-index: -1;
        position: relative;
        }
   `]
})

export class NewArticleComponent implements OnInit {
  articleForm: FormGroup;
  user;
  file: File;
  articles = [];
  isAdd;

  imagesFiles : Array<ImageFile> = [];


  constructor(private formBuilder: FormBuilder,
              private _articleService: ArticlesService,
              private _router: Router,
              private _usersService: UsersService

  ) {
    this._usersService.user.subscribe(value =>
      this.user = value);

  }

  ngOnInit() {
    this.articleForm = this.formBuilder.group({
      nameArticle: [],
      content: []
    });
  }


  readImageFile(imageFileClosure) {
    var reader = new FileReader();
    reader.onload = (event: any)=> {
      imageFileClosure.image = event.target.result;
    };
    reader.readAsDataURL(imageFileClosure.file);
  }

  fileChange(event) {
    for (var i = 0; i < event.target.files.length; i++) {
      let newFile = event.target.files.item(i);
      if (!this.imagesFiles.find( imageFile => newFile.name == imageFile.file.name)) {
        this.imagesFiles.push(new ImageFile({file : newFile, image: 'assets/download.gif'}));
      }
    }

    for (var i = 0; i < this.imagesFiles.length; i++) {
      let imageFile = this.imagesFiles[i];
      if (!((imageFile.image) instanceof Blob)) {
        this.readImageFile(imageFile);
      }
    }
  }

  deleteImage(image) {
    this.imagesFiles.splice(image, 1);
  }


  onSubmit() {

    var article = this.articleForm.value;
    var result;
    result = this._articleService.postArticle(article, this.imagesFiles);
    result.subscribe(x => {
      this._router.navigate(['/articles']);
    });
  }

}

