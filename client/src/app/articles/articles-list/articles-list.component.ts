import {Component, OnInit, Input} from '@angular/core';
//import { PSService } from "ng2-http-intercept";

import { ArticlesService } from "../../services/articles.service";
import { UsersService } from "../../services/users.service";
import {Router} from "@angular/router";
//var _ = require('underscore');
import * as _ from 'underscore';


@Component({
  templateUrl: './articles-list.component.html',
  styleUrls: ['./articles-list.component.css']

})

export class ArticlesListComponent implements OnInit {
  //@Input()
  user;
  articles : any;
  article;
  isLoggedIn;
  subscription;
  currentpage;
  currentpagetemp;
  articlesPerPage = 5;
  articlesTotal;
  pages;
  page;
  totalPages;
  newArticle;


  constructor(private _articlesService: ArticlesService,
              private _usersService: UsersService,
              private router: Router) {
    this.subscription = this._articlesService.onNewArticle.subscribe(value => {
      if (this.currentpage == this.totalPages || this.totalPages == 0) {
        if (this.articles.length < this.articlesPerPage) {
          this.articles.push(value.obj)
        } else {
          this.currentpage = this.currentpage + 1;
          this.setPage(this.currentpage);
        }
      }
    });
  }




  ngOnInit() {
    this.setPage(1);
    this.subscription = this._usersService.user.subscribe(value =>
      this.user = value);

    this.subscription = this._articlesService.onEditArticle.subscribe(value =>{
      this.newArticle = value;
      for (var i=0; i < this.articles.length; i++) {
        if (this.articles[i]._id === this.newArticle.obj._id) {
          this.articles[this.articles.indexOf(this.articles[i])]  = this.newArticle.obj;

        }
      }
    });
  }

  gotoDetail(article) {
    let link = ['articles', article._id];
    this.router.navigate(link);
  }

  setPage(currentpage: number) {

    this.currentpage = currentpage;
    this._articlesService.getArticles(currentpage, this.articlesPerPage)
      .subscribe(articles => {

        this.articles = articles["articles"];
        this.articlesTotal = articles["articlesTotal"];
        this.totalPages = Math.ceil(this.articlesTotal / this.articlesPerPage);


        var startPage, endPage;
        if (this.totalPages <= 10) {
          // less than 10 total pages so show all
          startPage = 1;
          endPage = this.totalPages;
        } else {
          // more than 10 total pages so calculate start and end pages
          if (this.currentpage <= 6) {
            startPage = 1;
            endPage = 10;
          } else if (this.currentpage + 4 >= this.totalPages) {
            startPage = this.totalPages - 9;
            endPage = this.totalPages;
          } else {
            startPage = this.currentpage - 5;
            endPage = this.currentpage + 4;
          }
        }

        this.pages = _.range(startPage, endPage + 1);
        this.currentpagetemp = currentpage;
      });
  }

  checkUser(article) {
    if (this.user && this.user._id === article.user){
      return true
    }
  }

  checkPermission() {
     if (this.user && this.user.role === 'admin') {
       return true
     }
  }

  editArticle(article) {

    let link = ['articles/edit', article._id];
    this.router.navigate(link)
      .then(res =>{
      this._articlesService.emitChange(article);

    });

  }

  deleteArticle(article) {

    this._articlesService.removeArticle(article._id, article, this.user._id)
      .subscribe(x => {
        this.router.navigate(['/articles']);
      });
    var index = this.articles.indexOf(article);
    this.articles.splice(index, 1)
  }


}
