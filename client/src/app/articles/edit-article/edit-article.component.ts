import {Component, OnInit, ElementRef} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';


import { ArticlesService } from "../../services/articles.service";
import {UsersService} from "../../services/users.service";
import {Input} from "@angular/core/src/metadata/directives";
import {ImageFile} from "../new-article/new-article.component";


@Component({
  templateUrl: './edit-article.component.html',
  styles: [`
        .delete-image {
          position: relative;
        }
        .glyphicon-remove {
           position: relative;
           top: -40px;
           left: -18px;
           z-index: 1;
           cursor:pointer;
        }
        .wrapper {
           z-index: -1;
           position: relative;
        }
        .picPreview {
           margin: 10px;
            float: left;
          
        }
    `]
})

export class EditArticleComponent implements OnInit {
    articleForm: FormGroup;
    user;
    file: File;
    articles;
    articleIndex;
    selectedArticle;
    article;
    imgLoaded;
    subscription;
    imagesToDeleteIds = [];
    hiddenImages ={};
    imagesFiles : Array<ImageFile> = [];



  constructor(private formBuilder: FormBuilder,
              private articlesService: ArticlesService,
              private router: Router,
              private route: ActivatedRoute,
              private _usersService: UsersService,
              private elRef: ElementRef) {
    this.article = {};
    this.subscription = this._usersService.user.subscribe(value =>
      this.user = value);

    this.articleForm = this.formBuilder.group({
      nameArticle: [],
      content: [],
      img: []
    });

  }

  ngOnInit() {
    this.route.params.subscribe(
      (params: any) => {
        this.articleIndex = params['id'];
        this.selectedArticle = this.articlesService.getArticle(this.articleIndex)
          .subscribe(article => {
            this.article = article;
            this.articleForm.patchValue(this.article);
          });
      });
  }

  loaded() {
    this.imgLoaded = true;
  }

  deleteEditedImage(image) {

    this.hiddenImages[image] = true;
    this.imagesToDeleteIds.push(image);
    //this.article.imagesId.splice(image, 1);
  }

  deleteAddedImage(image) {
    let index: number = this.imagesFiles.indexOf(image);
    this.imagesFiles.splice(index, 1);

  }

  fileChange(event) {
    var imagesCount = 3 - this.getTotalImages();
    if (imagesCount > event.target.files.length) {
      imagesCount = event.target.files.length;
    }
    if (imagesCount <= 0) {
      return;
    }
    for (var i = 0; i < imagesCount; i++) {
      let newFile = event.target.files.item(i);
      if (!this.imagesFiles.find( imageFile => newFile.name == imageFile.file.name)) {
        this.imagesFiles.push(new ImageFile({file : newFile, image: 'assets/download.gif'}));
      }
    }

    for (var i = 0; i < this.imagesFiles.length; i++) {
      let imageFile = this.imagesFiles[i];
      if (!((imageFile.image) instanceof Blob)) {
        this.readImageFile(imageFile);
      }
    }
  }

  readImageFile(imageFileClosure) {
    var reader = new FileReader();
    reader.onload = (event: any)=> {
      imageFileClosure.image = event.target.result;
    };
    reader.readAsDataURL(imageFileClosure.file);
  }

  getTotalImages() {
    if (this.article && this.article.imagesId) {
      var countImages = this.article.imagesId.length - this.imagesToDeleteIds.length + this.imagesFiles.length;
      return countImages;
    }
     else {
      return 0;
    }

  }

  onSubmit() {
    var article = this.articleForm.value;
    //this.article = article;
    var result;
    result = this.articlesService.updateArticle(this.article._id, article, this.imagesFiles, this.imagesToDeleteIds);
    result.subscribe(x => {
      this.router.navigate(['/articles']);
    });

  }
}

