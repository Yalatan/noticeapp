import {Component } from '@angular/core';
import {FormBuilder} from "@angular/forms";
import {UsersService} from "../../services/users.service";
import {ImageFile} from "../../articles/new-article/new-article.component";
import {ImageCropperComponent, CropperSettings} from 'ng2-img-cropper';
import {Router} from "@angular/router";


@Component({


  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css'],
  exportAs: 'child'
})

export class EditUserComponent {
  editUserForm;
  file: File;
  imageFile;
  subscription;
  user;

  constructor(private formBuilder: FormBuilder,
              private _usersService: UsersService,
              private router: Router
              ) {
    this.user = {};
    this.subscription = this._usersService.changeUserDetect.subscribe(value => {
      this.user = value;
      this.editUserForm.patchValue(this.user);
    });

    this.editUserForm = this.formBuilder.group({
      username: [],
      email: []

    });

  }

  readImageFile(imageFileClosure) {
    var reader = new FileReader();
    reader.onload = (event: any)=> {
      imageFileClosure.image = event.target.result;
    };
    reader.readAsDataURL(imageFileClosure.file);
  }

  fileChange(event) {
    let newFile = event.target.files[0];
    this.imageFile = new ImageFile({file : newFile, image: 'assets/download.gif'});
    this.readImageFile(this.imageFile);



  }
  onSubmit() {
    var user = this.editUserForm .value;
    //this.article = article;
    var result;

    result = this._usersService.updateUser(this.user._id, user, this.imageFile);
    result.subscribe(x => {
      this.router.navigate(['/users/profile/', this.user._id]);
    });

  }



}

