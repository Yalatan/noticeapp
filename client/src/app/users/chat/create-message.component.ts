import {ChatService} from '../../services/chat.service'
import {Component, Input} from '@angular/core';
import {UsersService} from "../../services/users.service";
import {ArticlesService} from "../../services/articles.service";
//import {Input} from "@angular/core/src/metadata/directives";

@Component({
  selector: 'create-message',
  templateUrl: './create-message.component.html'

})

export class CreateMessageComponent {
  //private submitted = false;
  private message;
  messagesArray = [];
  subscription;
  user;
  @Input() ownerPageId;
  anotherUserChatId;
  error_msg;
  selectedHistoryUserId;


  constructor(private chatService: ChatService,
              private _usersService: UsersService,
              private articlesService: ArticlesService) {
    this.subscription = this._usersService.user.subscribe(value =>
      this.user = value);

    chatService.messages.subscribe(msg => {
      //console.log("Response from websocket: " + msg);
      this.messagesArray.push(msg);
    });

    this.subscription = this._usersService.selectedUserSource
      .subscribe(value =>
        this.selectedHistoryUserId = value);
  }


  sendMessage(message) {
    if (this.user._id == this.ownerPageId && !this.selectedHistoryUserId) {
      this.error_msg = "You send message to yourself";

    } else if (this.user._id == this.ownerPageId && this.selectedHistoryUserId) {

      this.chatService.messages.next({
        message: message,
        recipient: this.selectedHistoryUserId,
        ownerPageId: this.ownerPageId
      });
      this.message = '';


    } else if (this.user._id != this.ownerPageId) {

      this.chatService.messages.next({message: message, recipient: this.ownerPageId, ownerPageId: this.ownerPageId});
      this.message = '';
    } else {

      this.anotherUserChatId = this.messagesArray[0].from._id;
      this.chatService.messages.next({
        message: message,
        recipient: this.anotherUserChatId,
        ownerPageId: this.ownerPageId
      });
      this.message = '';
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}

