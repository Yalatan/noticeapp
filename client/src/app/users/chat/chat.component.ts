import { Component, OnInit, Input } from '@angular/core';
import {  FormBuilder, FormGroup } from '@angular/forms';
import {Http, Headers} from "@angular/http";
import { Router } from '@angular/router';
import {UsersService} from "../../services/users.service";
import {ChatService} from "../../services/chat.service";
//import {Input} from "@angular/core/src/metadata/directives";




@Component({
  selector: 'chat',
  templateUrl: './chat.component.html'


})

export class ChatComponent {
  subscription;
  user;
  @Input() messages = [];

  constructor(
    private _usersService: UsersService,
    private chatService: ChatService) {

    this.subscription = this._usersService.user.subscribe(value => this.user = value);
    chatService.messages.subscribe(msg => {
      this.messages.push(msg);
    });
  }

}
