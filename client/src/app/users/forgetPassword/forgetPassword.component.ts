import { Component } from '@angular/core';
import { FormBuilder, FormGroup,  Validators } from '@angular/forms';
import {UsersService} from "../../services/users.service";
import {Router} from "@angular/router";



@Component({
  templateUrl: './forgetPassword.component.html'
})

export class ForgetPasswordComponent {
  restorePasswordForm: FormGroup;
  message;

  constructor(private fb: FormBuilder,
              private _usersService: UsersService,
              private _router: Router) {
    this.restorePasswordForm = this.fb.group({
      name: ['', Validators.required],
      email: ['', Validators.required]
    });
  }

  onSubmit() {
    this._usersService.restorePassword(this.restorePasswordForm.value)
      .subscribe(res => {

        this.message = res['message'];


      })
  }


}
