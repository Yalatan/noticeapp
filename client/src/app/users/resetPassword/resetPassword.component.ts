import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators, AbstractControl} from "@angular/forms";
import {UsersService} from "../../services/users.service";
import {Router, ActivatedRoute} from "@angular/router";


@Component({
  templateUrl: './resetPassword.component.html'
})

export class ResetPasswordComponent implements OnInit {
  resetPasswordForm: FormGroup;
  password;
  confirmPassword;
  message;
  token;
  user;

  constructor(private fb: FormBuilder,
              private _usersService: UsersService,
              private _router: Router,
              private route: ActivatedRoute) {
    this.resetPasswordForm = this.fb.group({
      passwords: this.fb.group({
        password: ['', [Validators.required]],
        confirmPassword: ['', [Validators.required]],
      }, {validator: ResetPasswordComponent.areEqual}),

    });


   }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get('token');
    this._usersService.sendToken(this.token)
    .subscribe(res => {
          this.user = res['user'];

    });
  }


     static areEqual(AC: AbstractControl) {
    let password = AC.get('password').value; // to get value in input tag
    let confirmPassword = AC.get('confirmPassword').value; // to get value in input tag
    if(password != confirmPassword) {
      console.log('false');
      AC.get('confirmPassword').setErrors( {MatchPassword: true} )
    } else {
      console.log('true');
      return null
    }
  }

  onSubmit() {
    this._usersService.resetPassword(this.user, this.resetPasswordForm.value['password'])
      // .subscribe(res => {
      //
      //
      // })
  }


}
