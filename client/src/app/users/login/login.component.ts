import { Component, OnInit } from '@angular/core';
import { FormsModule, ReactiveFormsModule, FormBuilder, FormGroup } from '@angular/forms';
import { Http, Headers} from "@angular/http";
import { Router } from '@angular/router';


import {UsersService} from "../../services/users.service";




@Component({
  templateUrl: './login.component.html'
})

export class LoginComponent implements OnInit {
  userForm: FormGroup;
  public token: string;
  currentuser;
  errMs;


  constructor(private formBuilder: FormBuilder,
              private _usersService: UsersService,
              private _router: Router ) {

    this.token = localStorage.getItem('token');
    this._usersService.user.subscribe(value =>
    {this.currentuser = value});
  }

  ngOnInit() {
    this.userForm = this.formBuilder.group({

      username: [],
      password: []
    });
  }



  onSubmit() {
    var user = this.userForm.value;
    var result = this._usersService.postUserLogin(user)
    .subscribe(res => {

        this._router.navigate(['/users/profile']);

      },
      error => {
        this.errMs = "failed to login";

      });

  }

}
