import {Component, ViewChild, OnInit } from '@angular/core';
import { FormsModule, ReactiveFormsModule, FormBuilder, FormGroup } from '@angular/forms';
import {HttpModule, Http, Headers} from "@angular/http";
import { Router } from '@angular/router';

//import {MODAL_DIRECTIVES, BS_VIEW_PROVIDERS} from 'ng2-bootstrap/ng2-bootstrap';

import {UsersService} from "../../services/users.service";

@Component({
  selector: 'login-modal',
  //directives: [MODAL_DIRECTIVES, CORE_DIRECTIVES, REACTIVE_FORM_DIRECTIVES],
  //viewProviders: [BS_VIEW_PROVIDERS],
  templateUrl: './login.modal.html',
  exportAs: 'child'
})

export class LoginModalComponent implements OnInit {
  userForm: FormGroup;
  constructor(private formBuilder: FormBuilder,
              private _usersService: UsersService,
              private _router: Router ) {}

  ngOnInit() {
    this.userForm = this.formBuilder.group({

      username: [],
      password: []
    });
  }

  @ViewChild('smModal') smModal;

  show() {
    this.smModal.show();
  }

  onSubmit() {

    var user = this.userForm.value;

    var result;
    result = this._usersService.postUserLogin(user);
    result.subscribe(x => {
      if (x) {
        this._router.navigate(['/articles-list']);
      }
    });

  }

}

