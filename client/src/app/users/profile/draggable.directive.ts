import {
  Directive, HostListener, EventEmitter, ElementRef, OnInit, ViewChild, AfterViewInit, Renderer2,
  AfterContentInit
} from '@angular/core';
import * as Rx from 'rxjs/Rx';
import 'rxjs/add/observable/fromEvent';


@Directive({
  selector: '[draggable]'

})
export class DraggableDirective  implements  OnInit, AfterContentInit  {
  @ViewChild('chatHeader', {read: ElementRef}) chatHeader:ElementRef;

  ngAfterViewInit() {
    console.log(this.chatHeader);
  }


  mousedrag;
  mouseup = new EventEmitter<MouseEvent>();
  mousedown = new EventEmitter<MouseEvent>();
  mousemove = new EventEmitter<MouseEvent>();



  @HostListener('document:mouseup', ['$event'])
  onMouseup(event: MouseEvent) {
    this.mouseup.emit(event);

  }

  @HostListener('document:mousedown', ['$event'])
  onMousedown(event: MouseEvent) {
    this.mousedown.emit(event);
    return true; // Call preventDefault() on the event
  }

  @HostListener('document:mousemove', ['$event'])
  onMousemove(event: MouseEvent) {
   // this.chatHeader.nativeElement.getBoundingClientRect().top;
    this.mousemove.emit(event);

  }



  constructor(public element: ElementRef,
              private rd: Renderer2) {
    this.element.nativeElement.style.position = 'absolute';
    this.element.nativeElement.style.cursor = 'pointer';

    this.mousedrag = this.mousedown.map(event => {
      var element = document.elementFromPoint(event.clientX, event.clientY);
      if (element && !element.classList.contains('chatHeader')) {
        return;
      }

      return {
        top: event.clientY - this.element.nativeElement.getBoundingClientRect().top,
        left: event.clientX - this.element.nativeElement.getBoundingClientRect().left,
      };

    })
      .flatMap(
        imageOffset => this.mousemove.map(pos => {
          if (imageOffset) {
            return {
              top: pos.clientY - imageOffset.top,
              left: pos.clientX - imageOffset.left
            }
          }
        })
          .takeUntil(this.mouseup)
      );
    //.takeUntil(this.mouseup);
  }

  ngOnInit() {
    this.mousedrag.subscribe({
      next: pos => {
        if (pos) {
          this.element.nativeElement.style.top = pos.top + 'px';
          this.element.nativeElement.style.left = pos.left + 'px';
        }
      }
    });
  }

  ngAfterContentInit() {
    console.log(this.chatHeader);
  }

}
