import {Component, OnInit} from '@angular/core';
import {  FormBuilder, FormGroup } from '@angular/forms';
import {Http, Headers} from "@angular/http";
import {Router, ActivatedRoute} from '@angular/router';
import {UsersService} from "../../services/users.service";
import {ArticlesService} from "../../services/articles.service";
import {ChatService} from "../../services/chat.service";
import { DraggableDirective } from './draggable.directive';



@Component({
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']

})

export class ProfileComponent {
  subscription;
  user;
  messageRequest;
  listUsers;
  chatMessages;
  selectedHistoryUserId;
  newMessagesArray = [];
  userIndex;
  currentUser;
  chat;
  show;


  constructor(
    private _usersService: UsersService,
    private articlesService: ArticlesService,
    private chatService: ChatService,
    private router: Router,
    private route: ActivatedRoute) {

    this.subscription = this._usersService.user.subscribe(value =>
    {this.currentUser = value});
    chatService.onNewMessage.subscribe(value => {
      this.chatMessages.push(value);
    });


  }

  messageForm() {
    this.messageRequest = true;
  }


  showChatHistory() {
    this.show = true;
    this._usersService.getChatUsers(this.user._id)
      .subscribe(x => {
        this.listUsers = x;
      });

  }

  hideChatHistory() {
    this.show = false;
  }

  getChatHistory(chatUserId) {
    this.chat = true;
    this.newMessagesArray = [];
    this.selectedHistoryUserId = chatUserId;

    this._usersService.getChatHistoryWithUser(chatUserId, this.user._id)
      .subscribe(x => {
        this.chatMessages = x;
      });
    this._usersService.changeSelectedUserId(this.selectedHistoryUserId);
  }


  ngOnInit() {

    this.route.params.subscribe(
      (params: any) => {
        this.userIndex = params['id'];
        this._usersService.getUserData(this.userIndex)
          .subscribe(user => {
            console.log('dsdsfd');
            this.user = user;
            if (this.user._id !== this.currentUser._id) {
              this._usersService.getGuestUserChatHistory(this.user._id, this.currentUser._id)
                .subscribe(x => {
                  this.chatMessages = x;
                });
            }
          });
      });

  }

  getChatHistoryForGuestUser() {

    // this._usersService.getGuestUserChatHistory(this.currentUser._id, this.user._id)
    //   .subscribe(x => {
    //     this.chatMessages = x;
    //   });
  }

  showChatHistoryForGuestUser(chatUserId) {
    this.chat = true;
    this.selectedHistoryUserId = chatUserId;


  }

    goToEditProfile(user) {
      let link = ['/users/profile/edit', user._id];
      this.router.navigate(link).then(res =>{
        this._usersService.emitChange(user);
    });
  }


}
