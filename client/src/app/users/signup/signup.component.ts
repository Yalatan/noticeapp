import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';

import {UsersService} from "../../services/users.service";


@Component({
  templateUrl: './signup.component.html'
})

export class SignupComponent implements OnInit {
  userForm: FormGroup;
  errorMessage: string;


  constructor(private formBuilder: FormBuilder,
              private _usersService: UsersService,
              private _router: Router ) {

  }

  ngOnInit() {
    this.userForm = this.formBuilder.group({
      // username: [],
      // email: [],
      // password: []
      username: ["", Validators.compose([Validators.required, Validators.minLength(3)])],
      email: ["", Validators.required],
      password: ["", Validators.required]
    });
  }

  onSubmit() {

    let user = this.userForm.value;

    var result;
    result = this._usersService.postUser(user)
    result.subscribe(x => {

        this._router.navigate(['/users/profile']);

      }
      ,
      error => {
        this.errorMessage = "Email is not unique";
        console.log(error.text());

      }
    );
  }


}

