import { Injectable } from '@angular/core';
import { HttpClient, HttpParams,  HttpHeaders, HttpResponse } from '@angular/common/http';
import 'rxjs/add/operator/map';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Subject} from "rxjs";
//import * as _ from 'underscore';


@Injectable()

export class ArticlesService {

  public getDataArticle = new BehaviorSubject<any>(null);
  public onNewArticle = new Subject<any>();
  public changeArticleDetect = new Subject<any>();
  public onEditArticle = new Subject<any>();


  constructor(private _http: HttpClient){}

  postArticle(article, imagesFiles) {
       let formData:FormData = new FormData();
       formData.append('article', JSON.stringify(article));
       for(var i = 0; i < imagesFiles.length; i++) {
       formData.append('images', imagesFiles[i].file, imagesFiles[i].file.name);
    }

       return this._http.post('http://localhost:3001/articles/new', formData)
         .map(res => {
           this.onNewArticle.next(res);
      });

  }




  getArticles(currentpage: number, articlesPerPage: number) {
    let Params = new HttpParams()
    .set('currentpage', currentpage.toString())
    .set('articlesPerPage', articlesPerPage.toString());
    return this._http.get('http://localhost:3001/articles/list', { params: Params});


  }


  getArticle(id) {
    return this._http.get('http://localhost:3001/articles/' + id)

  }



  emitChange(change: any) {
    this.changeArticleDetect.next(change);
  }

   updateArticle(id, article, imagesFiles, imagesToDeleteIds) {
        let formData:FormData = new FormData();
        formData.append('article', JSON.stringify(article));
        formData.append('imagesToDeleteIds', JSON.stringify(imagesToDeleteIds));

        for(var i = 0; i < imagesFiles.length; i++) {
          formData.append('images', imagesFiles[i].file, imagesFiles[i].file.name);
        }

        return this._http.post('http://localhost:3001/articles/edit/' + id, formData)
          .do(res => {
            this.onEditArticle.next(res);
          });
   }

  removeArticle(id, article, userId) {
    let body = JSON.stringify(article)
    return this._http.post('http://localhost:3001/articles/delete/' + id, body, userId)

  }

}
