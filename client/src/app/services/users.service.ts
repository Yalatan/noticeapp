import {Injectable, SkipSelf, Optional, Host} from '@angular/core';
import { HttpClient, HttpParams,  HttpHeaders, HttpResponse } from '@angular/common/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/startWith';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Subject} from "rxjs";
import { map } from 'rxjs/operators';


@Injectable()

export class UsersService {

  public user = new BehaviorSubject<any>(null);
  public selectedUserSource = new BehaviorSubject<any>(null);
  public changeUserDetect = new Subject<any>();
  public getUserProfileData = new BehaviorSubject<any>(null);
  token: string;


  constructor( private _http: HttpClient){}

  postUser(user) {
    return this._http.post('http://localhost:3001/users/signup', user)
      .do(res => {
        this.user.next(res);
        localStorage.setItem('token', res['token']);

      });
  }


  postUserLogin(user) {
       return this._http.post('http://localhost:3001/users/login', user)
       .do(res => {

        this.user.next(res['user']);
        localStorage.setItem('token', res['token']);

      });
  }

  postLogout() {
      return this._http.post('http://localhost:3001/users/logout', {});
  }

  getUser() {
    return this._http.get('http://localhost:3001/index/auth')
      .map(res => {
        this.user.next(res);
      });

  }

  getChatUsers(userId) {
    let Params = new HttpParams();
    Params.append('userId', userId);
    return this._http.post('http://localhost:3001/users/chatlist/', {params: Params})
  }


  getChatHistoryWithUser(chatUserId, userId) {
    let Params = new HttpParams();
    Params.append('chatUserId', chatUserId);
    Params.append('userId', userId);
    return this._http.post('http://localhost:3001/users/chatUser/', {params: Params})

  }

  changeSelectedUserId(selectedUserId) {
    this.selectedUserSource.next(selectedUserId);
  }

  getUserData(userId) {
    return this._http.post('http://localhost:3001/users/user/', {userId: userId})
      .do(res => {
        this.getUserProfileData.next(res)
      });
  }


  getGuestUserChatHistory(currentUser, ownerPageId) {
    let Params = new HttpParams();
    Params.append('currentUser', currentUser);
    Params.append('ownerPageId', ownerPageId);
    return this._http.post('http://localhost:3001/users/currentUserChat/', {params: Params})
  }

  emitChange(change: any) {
    this.changeUserDetect.next(change);
  }

  updateUser(id, user, imageFile) {
      let formData:FormData = new FormData();
      formData.append('user', JSON.stringify(user));
      if (imageFile) {
        formData.append('image', imageFile.file, imageFile.file.name);
      }

      return this._http.post('http://localhost:3001/users/edit/' + id, formData);

}

  // getChatDataForCurrentUser(currentUserId, OwnerPageUserId) {
  //   let data = new URLSearchParams();
  //   data.append('userId);
  //   return this._http.post('http://localhost:3001/users/chatlist/', data)
  //     .map(res => res.json());
  // }

  restorePassword(userData) {
    return this._http.post('http://localhost:3001/users/restorePassword', userData)

  }

  resetPassword (user, password) {
    let Params = new HttpParams();
    Params.append('user', user);
    Params.append('password', password);
    return this._http.post('http://localhost:3001/users/resetPassword', {params: Params})
  }

  sendToken(token) {
    return this._http.get('http://localhost:3001/users/sendToken/' + token)
}


 }
