import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs/Rx';
import {WebSocketService} from './websocket.service';
import 'rxjs/add/operator/map';
import * as Rx from 'rxjs/Rx';

const CHAT_URL = 'ws://localhost:3001';

export interface Message {
  recipient: string;
  message: string;
  ownerPageId: string;
}

@Injectable()
export class ChatService {
  public messages: Subject<Message>  = new Subject<Message>();
  public onNewMessage = new Subject<any>();

  constructor(wsService: WebSocketService) {


    this.messages = <Subject<Message>>wsService
      .connect(CHAT_URL)
      .map((response: MessageEvent): Message => {
        let data = JSON.parse(response.data);
        this.onNewMessage.next(data);
        return data;
      })
      .share();
  }
}
