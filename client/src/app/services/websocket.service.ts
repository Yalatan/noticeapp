//import * as Rx from 'rxjs/Rx';
import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {Observer} from "rxjs/Observer";
import {Observable} from 'rxjs/Observable';

@Injectable()
export class WebSocketService {
  private subject: Subject<MessageEvent>;
  //private subject: Rx.Subject<MessageEvent>;

  public connect(url): Subject<MessageEvent> {
    if(!this.subject) {
      this.subject = this.create(url);
    }

    return this.subject;
  }

  private create(url): Subject<MessageEvent> {

    let ws = new WebSocket(url, [localStorage.getItem('token')]);
    // let ws = new WebSocket (url, {
    //   headers : {
    //     token: localStorage.getItem('token')
    //   }
    // });


     let observable = Observable.create((obs: Observer<MessageEvent>) => {
      ws.onmessage = obs.next.bind(obs);
      ws.onerror = obs.error.bind(obs);
      ws.onclose = obs.complete.bind(obs);

      return ws.close.bind(ws);
    });

    let observer = {
      next: (data: Object) => {
        if (ws.readyState === WebSocket.OPEN) {
          ws.send(JSON.stringify(data));
        }
      },
    };

    return Subject.create(observer, observable);
  }
}
