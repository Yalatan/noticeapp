import { NoticeAppPage } from './app.po';

describe('notice-app App', () => {
  let page: NoticeAppPage;

  beforeEach(() => {
    page = new NoticeAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
